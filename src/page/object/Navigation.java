package page.object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.base.PageObject;

public class Navigation extends PageObject{
	public Navigation() {
		PageFactory.initElements(driver, this);
	}
	// Define elements ****************************************************
	
	@FindBy(xpath = ".//*[@class='menusubnav']")
	public WebElement navigationMenu;
	
	// Define method ****************************************************
	public void navigateToMenu(String menuName) {
		String _xpath = ".//*[@class='menusubnav']//following::*[text()='" + menuName + "']";
		WebElement menuItem = driver.findElement(By.xpath(_xpath));
		waitElementClickable(menuItem, 10);
		menuItem.click();
	}
	
}
