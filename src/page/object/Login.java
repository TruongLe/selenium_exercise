package page.object;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.base.PageObject;

public class Login extends PageObject{
	public static String USERID = "mngr193517";
	public static String PASSWD = "vEzArYb";
	
	public Login() {
		PageFactory.initElements(driver, this);
	}
	// Define elements ****************************************************
	@FindBy(xpath = ".//*[@name='uid']")
	public WebElement userId_txt;
	
	@FindBy(xpath = ".//*[@name='password']")
	public WebElement passwd_txt;
	
	@FindBy(xpath = ".//*[@name='btnLogin']")
	public WebElement login_btn;
	
	// Define methods ****************************************************
	public void login(String userId, String passwd) {
		System.out.println("Login with userId: " + userId + " and password:"+ passwd);
		userId_txt.sendKeys(userId);
		passwd_txt.sendKeys(passwd);
		
		login_btn.click();
	}
}
