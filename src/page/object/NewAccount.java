package page.object;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.base.PageObject;

public class NewAccount extends PageObject{
	public static String ACCOUNT_ID = "60075";
	
	public NewAccount() {
		PageFactory.initElements(driver, this);
	}
	
	// Define elements ****************************************************
	
	@FindBy(xpath = ".//*[@name='cusid']")
	public WebElement cusid_txt;
	
	@FindBy(xpath = ".//*[@name='selaccount']")
	public WebElement selaccount_dll;
	
	@FindBy(xpath = ".//*[@name='inideposit']")
	public WebElement inideposit_txt;
	
	@FindBy(xpath = ".//*[@name='button2' and @value='submit']")
	public WebElement submit_btn;
	
	@FindBy(xpath = ".//*[@name='reset']")
	public WebElement reset_btn;
	
	// Define methods ****************************************************
	public void InputNewAccount(HashMap<String,String> fieldData) {
		cusid_txt.sendKeys(fieldData.get("CustomerId"));
		dropDownListSelectText(selaccount_dll, fieldData.get("AccountType"));
		inideposit_txt.sendKeys("initialDeposit");
	}
	
	public void SubmitAddAccount() {
		submit_btn.click();
	}
}
