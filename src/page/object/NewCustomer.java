package page.object;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.base.PageObject;

public class NewCustomer extends PageObject{
	public static String CUSTOMER_ID = "28450";
	
	public NewCustomer() {
		PageFactory.initElements(driver, this);
	}
	// Define elements ****************************************************
	
	@FindBy(xpath = ".//*[@name='name']")
	public WebElement cusName_txt;
	
	@FindBy(xpath = ".//*[@type='radio' and @value='m']")
	public WebElement male_rad;
	
	@FindBy(xpath = ".//*[@type='radio' and @value='f']")
	public WebElement female_rad;
	
	@FindBy(xpath = ".//*[@id='dob']")
	public WebElement dob;
	
	@FindBy(xpath = ".//*[@name='addr']")
	public WebElement address_txt;
	
	@FindBy(xpath = ".//*[@name='city']")
	public WebElement city_txt;
	
	@FindBy(xpath = ".//*[@name='state']")
	public WebElement state_txt;
	
	@FindBy(xpath = ".//*[@name='pinno']")
	public WebElement pin_txt;
	
	@FindBy(xpath = ".//*[@name='telephoneno']")
	public WebElement mobileNum_txt;
	
	@FindBy(xpath = ".//*[@name='emailid']")
	public WebElement email_txt;
	
	@FindBy(xpath = ".//*[@name='password']")
	public WebElement password_txt;
	
	@FindBy(xpath = ".//*[@name='sub']")
	public WebElement submit_btn;
	
	@FindBy(xpath = ".//*[@name='res']")
	public WebElement reset_btn;
	
	// Define methods ****************************************************
	public void InputNewCustomer(HashMap<String,String> fieldData) {
		cusName_txt.sendKeys(fieldData.get("CustomerName"));
		if(fieldData.get("Gender") == "m"){
			male_rad.click();
		}
		else {female_rad.click();}
		
		dob.sendKeys(fieldData.get("DateofBirth"));
		address_txt.sendKeys(fieldData.get("Address"));
		city_txt.sendKeys(fieldData.get("City"));
		state_txt.sendKeys(fieldData.get("State"));
		pin_txt.sendKeys(fieldData.get("PIN"));
		mobileNum_txt.sendKeys(fieldData.get("MobileNumber"));
		email_txt.sendKeys(fieldData.get("Email"));
		password_txt.sendKeys(fieldData.get("Password"));
	}
	public void SubmitAddNew() {
		submit_btn.click();
	}
}
