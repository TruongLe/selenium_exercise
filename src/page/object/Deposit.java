package page.object;

import java.util.HashMap;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import pages.base.PageObject;

public class Deposit extends PageObject{
	public Deposit() {
		PageFactory.initElements(driver, this);
	}
	
	// Define elements ****************************************************
	@FindBy(xpath = ".//*[@name='accountno']")
	public WebElement accountno_txt;
	
	@FindBy(xpath = ".//*[@name='ammount']")
	public WebElement ammount_txt;
	
	@FindBy(xpath = ".//*[@name='desc']")
	public WebElement desc_txt;
	
	@FindBy(xpath = ".//*[@name='AccSubmit']")
	public WebElement submit_btn;
	
	@FindBy(xpath = ".//*[@name='res']")
	public WebElement reset_btn;

	
	// Define methods ****************************************************
	public void InputDeposit(HashMap<String,String> fieldData) {
		accountno_txt.sendKeys(fieldData.get("AccountNo"));
		ammount_txt.sendKeys(fieldData.get("Amount"));
		desc_txt.sendKeys(fieldData.get("Description"));
	}
	
	public void SubmitDeposit() {
		submit_btn.click();
	}
}
