package page.test;

import java.util.HashMap;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import page.object.Login;
import page.object.Navigation;
import page.object.NewAccount;
import pages.base.ConstantValues;
import pages.base.PageTest;
import utilites.Excel;

public class NewAccountTest  extends PageTest{
	private Login loginObject;
	private NewAccount newAccountObject;
	private Navigation navigationObject;
	
	public void initialPageObject() {
		loginObject = new Login();
		newAccountObject = new NewAccount();
		navigationObject = new Navigation();
	}
	
	@BeforeMethod
	public void beforeMethod(){
		initialPageObject();
	}

	public NewAccountTest() {
		initialPageObject();
	}
	
	@Test(dataProvider = "ExcelDataProviderGetData")
	public void AddNewAccount(HashMap<String, String> dataRow) {
		loginObject.login(loginObject.USERID, loginObject.PASSWD);
		
		navigationObject.navigateToMenu(ConstantValues.NAVIGATION_NEWACCOUNT);
		
		test.log(LogStatus.INFO, "**********************************************************************************");
		test.log(LogStatus.INFO, "Verify for case : " + dataRow.get("TestCaseId"));
		
		newAccountObject.InputNewAccount(dataRow);
		test.log(LogStatus.INFO, "Screent Shoot after input data" + shuttlePage());
		
		newAccountObject.SubmitAddAccount();
		
		//Call VerifyAddNewAccount() function
	}
	
	@DataProvider
	public static Object[][] ExcelDataProviderGetData(){
		Object[][] obj = Excel.readXSLXFileDataProvider("test-data/Account.xlsx", "AddNewAccount");
		return obj;
	}
}
