package page.test;

import page.object.Login;
import page.object.Navigation;
import pages.base.ConstantValues;
import pages.base.PageTest;
import utilites.Excel;

import java.util.HashMap;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import page.object.Deposit;

public class DepositTest extends PageTest {
	private Login loginObject;
	private Navigation navigationObject;
	private Deposit depositObject;
	
	public void initialPageObject() {
		loginObject = new Login();
		navigationObject = new Navigation();
		depositObject = new Deposit();
	}
	
	@BeforeMethod
	public void beforeMethod(){
		initialPageObject();
	}

	public DepositTest() {
		initialPageObject();
	}
	
	@Test(dataProvider = "ExcelDataProviderGetData")
	public void AddNewCustomer(HashMap<String, String> dataRow) {
		loginObject.login(loginObject.USERID, loginObject.PASSWD);
		
		navigationObject.navigateToMenu(ConstantValues.NAVIGATION_DEPOSIT);
		
		test.log(LogStatus.INFO, "**********************************************************************************");
		test.log(LogStatus.INFO, "Verify for case : " + dataRow.get("TestCaseId"));
		System.out.println("Verify for case : " + dataRow.get("TestCaseId"));
		
		depositObject.InputDeposit(dataRow);
		test.log(LogStatus.INFO, "Screent Shoot after input data" + shuttlePage());
		
		depositObject.SubmitDeposit();
		
		//Call VerifyNewDeposit() function 
	}
	
	@DataProvider
	public static Object[][] ExcelDataProviderGetData(){
		Object[][] obj = Excel.readXSLXFileDataProvider("test-data/Deposit.xlsx", "Deposit");
		return obj;
	}

}
