package page.test;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.LogStatus;

import page.object.Login;
import page.object.Navigation;
import page.object.NewCustomer;
import pages.base.PageTest;
import utilites.Excel;
import pages.base.ConstantValues;

public class NewCustomerTest extends PageTest{
	private Login loginObject;
	private NewCustomer newCustomerObject;
	private Navigation navigationObject;
	
	public void initialPageObject() {
		loginObject = new Login();
		newCustomerObject = new NewCustomer();
		navigationObject = new Navigation();
	}
	
	@BeforeMethod
	public void beforeMethod(){
		initialPageObject();
	}

	public NewCustomerTest() {
		initialPageObject();
	}

	/*
	 * public void Login() { test.log(LogStatus.INFO, "Login into home page");
	 * test.log(LogStatus.INFO,
	 * "**********************************************************************************"
	 * ); Assert.assertEquals(driver.getCurrentUrl(), "http://demo.guru99.com/v4/");
	 * Assert.assertEquals(driver.getTitle(), "Guru99 Bank Home Page");
	 * test.log(LogStatus.PASS, "Home page title is correctly.");
	 * test.log(LogStatus.INFO, "Screen shoot" + shuttlePage());
	 * 
	 * loginObject.login(loginObject.USERID, loginObject.PASSWD);
	 * Assert.assertEquals(driver.getCurrentUrl(),
	 * "http://demo.guru99.com/v4/manager/Managerhomepage.php"); }
	 */
	
	@Test(dataProvider = "ExcelDataProviderGetData")
	public void AddNewCustomer(HashMap<String, String> dataRow) {
		loginObject.login(loginObject.USERID, loginObject.PASSWD);
		
		navigationObject.navigateToMenu(ConstantValues.NAVIGATION_NEWCUSTOMER);
		
		test.log(LogStatus.INFO, "**********************************************************************************");
		test.log(LogStatus.INFO, "Verify for case : " + dataRow.get("TestCaseId"));
		System.out.println("Verify for case : " + dataRow.get("TestCaseId"));
		
		newCustomerObject.InputNewCustomer(dataRow);
		test.log(LogStatus.INFO, "Screent Shoot after input data" + shuttlePage());
		
		newCustomerObject.SubmitAddNew();
		
		///Call VerifyAddNewCustomer() function
	}
	
	@DataProvider
	public static Object[][] ExcelDataProviderGetData(){
		Object[][] obj = Excel.readXSLXFileDataProvider("test-data/Customer.xlsx", "AddNewCustomer");
		return obj;
	}
}
