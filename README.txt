Automation Test Exercise

- Objective:
Write a suite of three automated browser tests for demo banking app at: http://demo.guru99.com/v4/

- Tasks:
Verify 3 functions of Demo Banking system:
1. Verify new customer can be created. DONE (basically) 
2. Verify to create new account based on the customer just created above. DONE (basically)
3. Verify deposit function work fine with the account just created. DONE (basically) 

- Prequisites
OS: Windows
Browser: Chrome with latest version

Selenium framework with TestNG<br>

- The project is a simple framework build for selenium and testNG.
The project uses:
- Page object model.
- Extent report
- Excel for test data.

- Information of me
Le Ngoc Truong - Automation Tester
Email: lntruongkh@gmail.com
Phone: 0986308044
Linkedin:https://www.linkedin.com/in/truongle76/
